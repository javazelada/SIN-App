import HomePage from './pages/home.vue';
import AboutPage from './pages/about.vue';
import FormPage from './pages/form.vue';
import DynamicRoutePage from './pages/dynamic-route.vue';
import NotFoundPage from './pages/not-found.vue';
import LoginPage from './pages/login.vue';
import ListaPage from './pages/lista.vue'
import MainPage from './pages/main-page.vue';
import ItemPage from './pages/item.vue'

import PanelLeftPage from './pages/panel-left.vue';
import PanelRightPage from './pages/panel-right.vue';

export default [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/main-page/',
    component: MainPage
  },
  {
    path: '/item/',
    component: ItemPage
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage,
  },
  {
    path: '/panel-right/',
    component: PanelRightPage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/form/',
    component: FormPage,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/login/',
    component: LoginPage,
  },
  {
    path: '/list/',
    component: ListaPage,
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  }
];
